package com.mbds.android.nfc.code;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mbds.android.nfc.code.databinding.FragmentFormInsertDataBinding;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FormInsertDataFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FormInsertDataFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private FragmentFormInsertDataBinding fragmentFormInsertDataBinding;

    // TODO: Rename and change types of parameters
    private String mParam;

    public FormInsertDataFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment FormInsertDataFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FormInsertDataFragment newInstance(String param1) {
        FormInsertDataFragment fragment = new FormInsertDataFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentFormInsertDataBinding = FragmentFormInsertDataBinding.inflate(inflater, container, false );
        View view = fragmentFormInsertDataBinding.getRoot();
        return  view;
    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        switch (mParam){
            case ChoiceListFragment.ARG_WEB : {
                this.fragmentFormInsertDataBinding.idLibelle.setText(R.string.libelle_web);
                break;
            }

            case ChoiceListFragment.ARG_CONTACT : {
                this.fragmentFormInsertDataBinding.idLibelle.setText(R.string.libelle_contact);
                break;
            }

            case ChoiceListFragment.ARG_TEXTE  : {
                this.fragmentFormInsertDataBinding.idLibelle.setText(R.string.libelle_text);
                break;
            }
        }

        //
        fragmentFormInsertDataBinding.idBouttonValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), NFCWriterActivity.class);
                intent.putExtra("mParam", mParam);
                intent.putExtra("data", fragmentFormInsertDataBinding.idLibelle.getText().toString());
                startActivity(intent);
            }
        });
    }
}