package com.mbds.android.nfc.code;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.mbds.android.nfc.code.databinding.WriteTagLayoutBinding;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Locale;

public class NFCWriterActivity extends Activity {
    private NfcAdapter nfcAdapter;
    private PendingIntent pendingIntent;
    private String type_data;
    private String libelle;

    private WriteTagLayoutBinding writeTagLayoutBinding;

    // TODO Analyser le code et comprendre ce qui est fait
    // TODO Ajouter un formulaire permettant à un utilisateur d'entrer le texte à mettre dans le tag
    // TODO Le texte peut être 1) une URL 2) un numéro de téléphone 3) un plain texte
    // TODO Utiliser le view binding
    // TODO L'app ne doit pas crasher si les tags sont mal formattés

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        writeTagLayoutBinding = WriteTagLayoutBinding.inflate(getLayoutInflater());
        View view = writeTagLayoutBinding.getRoot();
        setContentView(view);
       //setContentView(R.layout.write_tag_layout);

        //Get default NfcAdapter and PendingIntent instances
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        // check NFC feature:
        if (nfcAdapter == null) {
            // process error device not NFC-capable…

        }
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).
                addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        // single top flag avoids activity multiple instances launching

        // Recuperation des valeurs
        type_data = getIntent().getStringExtra("mParam");
        libelle = getIntent().getStringExtra("data");
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Enable NFC foreground detection
        if (nfcAdapter != null) {
            if (!nfcAdapter.isEnabled()) {
                // process error NFC not activated…
            }
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        //Disable NFC foreground detection
        if (nfcAdapter != null) {
            nfcAdapter.disableForegroundDispatch(this);
        }

    }


    @Override
    public void onNewIntent(Intent intent) {

        //Get the Tag object:
        //===================
        // retrieve the action from the received intent
        String action = intent.getAction();
        // check the event was triggered by the tag discovery
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            // get the tag object from the received intent
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            // create the NDEF mesage:
            //========================
            // dimension is the int number of entries of ndefRecords:
            int dimension = 1;
            NdefRecord[] ndefRecords = new NdefRecord[dimension];
            NdefRecord ndefRecord = null;
            NdefMessage ndefMessage = null;
            String data_to_write= libelle;

            switch (type_data){
                
                case ChoiceListFragment.ARG_WEB :{
                    Toast.makeText(this, "In web", Toast.LENGTH_SHORT).show();
                        // Example with an URI NDEF record:
                      //  uriTxt = "http://www.mbds-fr.org"; // your URI in String format
                        ndefRecord = NdefRecord.createUri(data_to_write);
                    break;
                }

                case ChoiceListFragment.ARG_CONTACT : {

                    Toast.makeText(this, "In Contact", Toast.LENGTH_SHORT).show();
                    //NDEF record WELL KNOWN type (NFC Forum): TEXT
                    //=============================================
                    byte[] lang = new byte[0];
                    byte[] data = new byte[0];
                    int langeSize = 0;

                    try {
                        lang = Locale.getDefault().getLanguage().getBytes("UTF-8");
                        langeSize = lang.length;
                        data = new byte[0];
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    try {
                        data = data_to_write.getBytes("UTF-8");
                        int dataLength = data.length;
                        ByteArrayOutputStream payload = new ByteArrayOutputStream(1 + langeSize + dataLength);
                        payload.write((byte) (langeSize & 0x1F));
                        payload.write(lang, 0, langeSize);
                        ndefRecord = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
                                NdefRecord.RTD_TEXT, new byte[0],
                                payload.toByteArray());
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    break;
                }

                case ChoiceListFragment.ARG_TEXTE : {
                    Toast.makeText(this, "In text", Toast.LENGTH_SHORT).show();
                    //Create NDEF message record type MIME:
                    //=====================================
                    String mimeType = "application/mbds.android.nfc"; // your MIME type
                    ndefRecord = NdefRecord.createMime(mimeType,
                            data_to_write.getBytes(Charset.forName("US-ASCII")));
                }

            }

            // Add the record to the NDEF message:
            ndefRecords[0] = ndefRecord;
            ndefMessage = new NdefMessage(ndefRecords);

            //check and write the tag received:
            //=================================
            // check the targeted tag the memory size and is the tag writable
            Ndef ndef = Ndef.get(tag);
            int size = ndefMessage.toByteArray().length;

            if (ndef != null) {
                try {
                    ndef.connect();
                    if (!ndef.isWritable()) {
                        // tag is locked in writing!
                    }
                    if (ndef.getMaxSize() < size) {
                        // manage oversize!
                    }
                    // write the NDEF message on the tag
                    ndef.writeNdefMessage(ndefMessage);
                    ndef.close();
                    Toast.makeText(this, "Message écrit avec succès", Toast.LENGTH_SHORT).show();
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (FormatException e2) {
                    e2.printStackTrace();
                }
            }

            //check and write the tag received at activity:
            //=============================================
            // is the tag formatted?
            if (ndef == null) {
                NdefFormatable format = NdefFormatable.get(tag);
                if (format != null) {
                    // can you format the tag?
                    try {
                        format.connect();
                        //Format and write the NDEF message on the tag
                        format.format(ndefMessage);
                        //Example of tag locked in writing:
                        //formatable.formatReadOnly(message);
                        format.close();
                        Toast.makeText(this, "Message écrit avec succès ", Toast.LENGTH_SHORT).show();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (FormatException e2) {
                        e2.printStackTrace();
                    }
                }
            }

        }
    }
}
