package com.mbds.android.nfc.code;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import androidx.core.content.ContextCompat;

import com.mbds.android.nfc.code.databinding.ReadTagLayoutBinding;

import java.io.UnsupportedEncodingException;

import static android.Manifest.permission.CALL_PHONE;

public class NFCReaderActivity extends Activity {

    private NfcAdapter nfcAdapter;
    private PendingIntent pendingIntent;
    public static String TAG = "TAG";
    public ReadTagLayoutBinding readTagLayoutBinding;



    // TODO Lire le contenu d'un tag et effectuer les actions en fonction du contenu
    // TODO Si c'est un numéro de téléphone, lancer un appel
    // TODO Si c'est une page web lancer un navigateur pour afficher la page
    // TODO Sinon afficher le contenu dans la textviewx
    // TODO utiliser le view binding
    // TODO Faire en sorte que l'app ne crash pas si le tag est vierge ou mal formatté
    // TODO Demander à l'utilisateur d'activer le NFC, s'il ne l'est pas

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        readTagLayoutBinding = ReadTagLayoutBinding.inflate(getLayoutInflater());
        View view= readTagLayoutBinding.getRoot();
        //setContentView(R.layout.read_tag_layout);
        setContentView(view);



        //Get default NfcAdapter and PendingIntent instances
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        // check NFC feature:
        if (nfcAdapter == null) {
            // process error device not NFC-capable…
            Toast.makeText(this, "NFC N'EST PAS ACTIVE", Toast.LENGTH_LONG).show();
            finish();
        }
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).
                addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        // single top flag avoids activity multiple instances launching


    }

    @Override
    protected void onResume() {
        super.onResume();
        //Enable NFC foreground detection
        if (nfcAdapter != null) {
            if (!nfcAdapter.isEnabled()) {
                // process error NFC not activated…
                Toast.makeText(this, "NFC N'EST PAS ACTIVE", Toast.LENGTH_LONG).show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    startActivity(new Intent(android.provider.Settings.ACTION_NFC_SETTINGS));
                }else{
                    startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
                }
            }
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
        } else {
            Toast.makeText(this, "NFC N'EST PAS ACTIVE", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Disable NFC foreground detection
        if (nfcAdapter != null) {
            nfcAdapter.disableForegroundDispatch(this);
        }

    }

    @Override
    public void onNewIntent(Intent intent) {

        //Get the Tag object:
        //===================
        // retrieve the action from the received intent
        String action = intent.getAction();
        // check the event was triggered by the tag discovery
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            String message = "AUCUNE INFORMATION TROUVEE SUR LE TAG !!!";

            // get the tag object from the received intent
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            // Verifier si on a lu correctement les informations sur le Tag
            if(tag==null){
                Toast.makeText(this, "Veuillez verifier si vous avez correctement dejas ecrit sur le Tag ",
                        Toast.LENGTH_LONG).show();
                return;
            }

            //Get the Tag object information:
            //===============================
            // get the UTD from the tag
            byte[] uid = tag.getId();

            message = "Tag détecté UID : " + uid.toString();

            // get the technology list from the tag
            String[] technologies = tag.getTechList();
            // bit reserved to an optional file content descriptor
            int content = tag.describeContents();
            // get NDEF content
            Ndef ndef = Ndef.get(tag);

            // is the tag writable?
            boolean isWritable = ndef.isWritable();

            if (isWritable)
                message = message + " réinscriptible";
            else
                message = message + " non inscriptible";

            // can the tag be locked in writing?
            boolean canMakeReadOnly = ndef.canMakeReadOnly();

            if (canMakeReadOnly)
                message = message + ", verrouillable en écriture";
            else
                message = message + ", non verrouillable en écriture";

            //: get NDEF records:
            //===================
            String recordTxt="";
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            // check if the tag contains an NDEF message
            if (rawMsgs != null && rawMsgs.length != 0) {
                // instantiate a NDEF message array to get NDEF records
                NdefMessage[] ndefMessage = new NdefMessage[rawMsgs.length];
                // loop to get the NDEF records
                for (int i = 0; i < rawMsgs.length; i++) {

                    ndefMessage[i] = (NdefMessage) rawMsgs[i];
                    for (int j = 0; j < ndefMessage[i].getRecords().length; j++) {
                        NdefRecord ndefRecord = ndefMessage[i].getRecords()[j];

                        //parse NDEF record as String:
                        //============================
                        byte[] payload = ndefRecord.getPayload();
                        String encoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTf-8";
                        int languageSize = payload[0] & 0063;
                        try {
                            recordTxt = new String(payload, languageSize + 1,
                                    payload.length - languageSize - 1, encoding);

                            message = message + ", NDEF MESSAGE : " + recordTxt;

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                        Log.d("NFC", "onNewIntent: "+message);

                        //check NDEF record TNF:
                        //======================
                        switch(ndefRecord.getTnf()) {

                            case NdefRecord.TNF_ABSOLUTE_URI:
                                // manage NDEF record as an URI object
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(recordTxt));
                                startActivity(browserIntent);
                                break;

                            case NdefRecord.TNF_WELL_KNOWN:
                                // manage NDEF record as the type is:
                                // contact (business card), phone number, email…
                                Intent callIntent = new Intent();
                                callIntent.setData(Uri.parse("tel:"+ recordTxt));
                                if (ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                                    callIntent.setAction(Intent.ACTION_CALL);
                                    startActivity(callIntent);
                                } else {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{CALL_PHONE}, 1);
                                    }else{
                                        callIntent.setAction(Intent.ACTION_DIAL);
                                        startActivity(callIntent);
                                    }
                                }
                                break;

                            default:
                                readTagLayoutBinding.txtView1.setText(recordTxt);
                                readTagLayoutBinding.txtView1.setTextSize(32);
                                readTagLayoutBinding.txtView1.setTextColor(Color.RED);

                               // readTagLayoutBinding.txtView1.set
                                // manage NDEF record as text…
                        }
                    }
                }
            }

        }
    }
}
