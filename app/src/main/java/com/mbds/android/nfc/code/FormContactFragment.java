package com.mbds.android.nfc.code;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
/*import androidx.navigation.fragment.NavHostFragment;*/

public class FormContactFragment extends Fragment {

    private static final String ARG_PARAM = "paramPass";

    public FormContactFragment(){

    }

    public static FormContactFragment newInstance(String data){
        FormContactFragment fragment = new FormContactFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_contact, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

  /*      view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FormContactFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });*/
    }
}