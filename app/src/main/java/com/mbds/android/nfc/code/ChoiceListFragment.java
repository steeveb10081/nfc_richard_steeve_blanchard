package com.mbds.android.nfc.code;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.mbds.android.nfc.code.databinding.FragmentChoiceListBinding;

public class ChoiceListFragment extends Fragment {
    private static final String ARG_PARAM = "paramPass";
    private String dataReturn;
    private FragmentChoiceListBinding fragmentChoiceListBinding;

    public static final String ARG_WEB= "WEB";
    public static final String ARG_CONTACT= "CONTACT";
    public static final String ARG_TEXTE= "TEXTE";

    public ChoiceListFragment(){

    }

    public static ChoiceListFragment newInstance(String data){
        ChoiceListFragment fragment = new ChoiceListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataReturn = (String) getArguments().getString(ARG_PARAM);
        }
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        fragmentChoiceListBinding = FragmentChoiceListBinding.inflate(inflater, container, false);
        View view = fragmentChoiceListBinding.getRoot();
       // return inflater.inflate(R.layout.fragment_choice_list, container, false);
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fragmentChoiceListBinding.idChoiceWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frament,  FormInsertDataFragment.newInstance(ARG_WEB)).commit();

            }
        });

        fragmentChoiceListBinding.idChoiceCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frament,  FormInsertDataFragment.newInstance(ARG_CONTACT)).commit();

            }
        });

        fragmentChoiceListBinding.idChoiceText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frament,  FormInsertDataFragment.newInstance(ARG_TEXTE)).commit();

            }
        });
    }
}